import pytest
from filestorage import MyFSStorage
from db_manager import MyDBManager


@pytest.fixture(scope='session')
def file_storage():

    return MyFSStorage(root='test_db')


@pytest.fixture(scope='session')
def db_manager(file_storage):

    return MyDBManager(storage=file_storage)