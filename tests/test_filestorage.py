import pytest
import os

def test_check_valid_key(file_storage):
    """тест: тестируем получение"""

    # тест 1: ключ только из цифр
    with pytest.raises(ValueError):
        file_storage._check_valid_key(111)

    # тест 2: ключ не соответвствует регулярному выражению
    with pytest.raises(ValueError):
        file_storage._check_valid_key('\377')

def test_build_filename(file_storage):
    """тест: получение имени файла"""

    key = 'key255'

    assert os.path.abspath(os.path.join(file_storage.root, key)) == file_storage._build_filename(key)

def test_ensure_dir_exists(file_storage):
    """тест: проверка существования директории"""
    path = '//'

    assert file_storage._ensure_dir_exists(path) is None

def test_fix_permissions(file_storage):
    """тест: проверка разрешений на запись файла"""

    filename = 'test_file'
    abs_filename = os.path.join(os.path.abspath(file_storage.root), filename)

    assert file_storage.perms is None

    file_storage.put(filename, None)

    file_storage._fix_permissions(abs_filename)

    current_umask = os.umask(0)

    perm = 0o666 & (0o777 ^ current_umask)

    assert oct(os.stat(abs_filename).st_mode)[-3:] == oct(perm)[-3:]

    file_storage.delete(filename)