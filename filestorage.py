import os
import re
from io import BytesIO

VALID_NOT_NUM = r"""\`\!"#$%&'()+,-.<=>?@[]^_{}~"""
VALID_KEY_REXP = f'^[{re.escape(VALID_NOT_NUM)}0-9a-zA-Z]+$'
VALID_KEY = re.compile(VALID_KEY_REXP)


class MyFSStorage:
    def __init__(self, root: str, perms: str = None):
        self.root = root
        self.perms = perms
        self.buf_size = 1024 * 1024

    def __getitem__(self, item):
        return self.get(item)

    def __setitem__(self, key, value):
        return self.put(key, value)

    def _check_valid_key(self, key):
        if not isinstance(key, str):
            raise ValueError('%r is not a valid key type' % key)
        if not VALID_KEY.match(key):
            raise ValueError('%r contains illegal characters' % key)

    def _build_filename(self, key: str) -> str:
        return os.path.abspath(os.path.join(self.root, key))

    def _ensure_dir_exists(self, path: str):
        if not os.path.isdir(path):
            os.makedirs(path)

    def _fix_permissions(self, filename):
        current_umask = os.umask(0)
        os.umask(current_umask)

        perm = self.perms
        if perm is None:
            perm = 0o666 & (0o777 ^ current_umask)

        os.chmod(filename, perm)

    def _put_file(self, key: str, data: bytes):
        buf_size = self.buf_size
        file = BytesIO(data)

        target = self._build_filename(key)
        self._ensure_dir_exists(os.path.dirname(target))

        with open(target, 'wb') as f:
            while True:
                buf = file.read(buf_size)
                f.write(buf)
                if len(buf) < buf_size:
                    break

        if self.perms is not None:
            self._fix_permissions(target)

        return key

    def put(self, key, data):
        self._check_valid_key(key)
        bdata = str(data).encode()

        return self._put_file(key, bdata)

    def _open(self, key):
        try:
            f = open(self._build_filename(key), 'rb')
            return f
        except:
            raise KeyError(key)

    def open(self, key):
        self._check_valid_key(key)
        return self._open(key)

    def _get_file(self, key, file):
        buf_size = 1024 * 1024

        source = self.open(key)
        try:
            while True:
                buf = source.read(buf_size)
                file.write(buf)

                if len(buf) < buf_size:
                    break
        finally:
            source.close()

    def _get(self, key: str) -> bytes:
        buf = BytesIO()

        self._get_file(key, buf)

        return buf.getvalue()

    def get(self, key: str) -> str:
        self._check_valid_key(key)
        return self._get(key).decode()

    def _delete(self, key):
        try:
            targetname = self._build_filename(key)
            if os.path.exists(targetname):
                os.unlink(targetname)
            else:
                raise KeyError
        except:
            raise OSError

    def delete(self, key):
        self._check_valid_key(key)
        return self._delete(key)

    def keys(self, prefix=''):
        root = os.path.abspath(self.root)
        result = []
        for dp, dn, fn in os.walk(root):
            for f in fn:
                key = os.path.join(dp, f)[len(root) + 1:]
                if key.startswith(prefix):
                    result.append(key)
        return result

    def iter_keys(self, prefix=''):
        return iter(self.keys(prefix))

    def get_keys_by_value(self, value: str) -> list[str]:
        result = [key for key in self.iter_keys() if self.get(key) == value]
        return result


