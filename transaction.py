import os


class MyTransaction:
    def __init__(self, manager):
        self.manager = manager
        self.parent = None
        self.nested = None

        self.todo = {}

    def commit(self):
        if self.nested:
            self.nested.commit()

        for key, val in self.todo.items():
            if val is None:
                self.manager.storage.delete(key)
            else:
                self.manager.storage.put(key, val)
        self.todo.clear()

        self.manager._txn = self.parent

    def rollback(self):
        if self.nested:
            self.nested.rollback()

        self.todo.clear()

        self.manager._txn = self.parent


class MyTransactionManager:
    def __init__(self, storage):
        self.storage = storage
        self._txn = None
        self.set_lock = False

    def __setattr__(self, key, value):
        if key == '_txn' and value is None and hasattr(self, 'set_lock') and self.set_lock:
            # все транзакции закрылись снимаем lock
            path_to_lock = f'{os.path.abspath(self.storage.root)}.lock'
            if os.path.exists(path_to_lock):
                os.unlink(path_to_lock)

            self.set_lock = False

        super().__setattr__(key, value)

    def begin(self):
        if not self.set_lock:
            # пытаемся поставить lock
            path_to_lock = f'{os.path.abspath(self.storage.root)}.lock'
            if os.path.exists(path_to_lock):
                raise Exception('Заблокировано другой транзакцией')
            else:
                # мы поставили lock
                with open(path_to_lock, 'w') as f:
                    f.write('lock')

                self.set_lock = True

        txn = MyTransaction(manager=self)
        if self._txn is not None:
            self._txn.nested = txn
            txn.parent = self._txn

        self._txn = txn
        return txn

    def __enter__(self):
        return self.begin()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self._txn.commit()
        else:
            self._txn.rollback()

    def put(self, key, value):
        self._txn.todo[key] = value

    def delete(self, key):
        self._txn.todo[key] = None
