from http.server import BaseHTTPRequestHandler
from http.server import HTTPServer
import json
# from urllib.parse import urlparse

from filestorage import MyFSStorage
from db_manager import MyDBManager

fs_store = MyFSStorage(root='db')
manager = MyDBManager(storage=fs_store)


class MyHttpHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

        pos = self.path.find('/item')
        if pos > -1:
            key = self.path[(pos + len('/item') + 1):]
            try:
                if not key:
                    self.wfile.write(json.dumps(
                        {key: manager[key] for key in manager.storage.keys()}
                    ).encode())
                else:
                    value = manager[key]
                    self.wfile.write(json.dumps({key: value}).encode())
                return
            except:
                self.send_response(500, f'Невозможно получить значение по ключу {key}')

    def do_POST(self):
        if self.path.endswith('/item'):
            content_len = int(self.headers['content-length'])
            post_body = self.rfile.read(content_len)
            data: dict = json.loads(post_body)

            for key, val in data.items():

                try:
                    manager[key] = val
                except:
                    self.send_response(500, f'Невозможно получить значение по ключу {key}')

            self.send_response(200)
            self.end_headers()
            self.wfile.write(json.dumps({'received': 'ok'}).encode())
            return

def run(server_class=HTTPServer, handler_class=MyHttpHandler):
  server_address = ('', 8000)
  httpd = server_class(server_address, handler_class)
  try:
      httpd.serve_forever()
  except KeyboardInterrupt:
      httpd.server_close()

if __name__ == '__main__':
    run()