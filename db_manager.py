from transaction import MyTransactionManager


class MyDBManager:
    def __init__(self, storage):
        self.storage = storage
        self.txn_manager = MyTransactionManager(storage=storage)

    def put(self, key, value):
        if self.txn_manager._txn is None:
            self.txn_manager.begin()

        self.txn_manager.put(key, value)
        self.storage.put(key, value)

    def delete(self, key):
        if self.txn_manager._txn is None:
            self.txn_manager.begin()

        self.txn_manager.delete(key)
        self.storage.delete(key)

    def __getitem__(self, item):
        return self.storage[item]

    def __setitem__(self, key, value):
        self.storage[key] = value

