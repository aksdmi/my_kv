FROM python:3.10

RUN mkdir -p /deploy/app
WORKDIR /deploy/app
COPY ./db_manager.py .
COPY ./filestorage.py .
COPY ./server.py .
COPY ./transaction.py .

CMD ["python3", "server.py"]
